<?php
	$server = 'localhost:3306';
	$username = 'root';
	$password = 'root'; 
	$database = 'books_db';
	$table = '`books`';
	
	$connection = mysqli_connect($server, $username, $password);
	
	if(!$connection){
		die("Failed to connect: ".mysqli_connect_error());
	}
	
	mysqli_select_db($connection, $database);
	
	/*Books with the same title will appear only one time on the drop-down list.
	 However, the server will return a JSON Array which contains information about
	  every book with the same title.
	*/
	$SQLQuery = 'SELECT DISTINCT `title` FROM '.$table.' ORDER BY `title`';	
	
	$result = mysqli_query($connection, $SQLQuery);
	
	if(!$result){
		die("An error occured.");
	}
	
	$rows = array();
	
	while($row = mysqli_fetch_array($result)){
		$rows[] = $row;
	}
	
	mysqli_close($connection);
	
	echo json_encode($rows);
?>